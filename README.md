ESP8266 MOBILE CONTROLLED ADVENT WREATH
            (FOR SCHOOLS)
with auto mode and end-of-break alarm
by David Voß (David.Voss@bkukr.de)
Berufskolleg Uerdingen, Krefeld

In Germany the custom of having an advent wreath is very common. Unfortunately if you want to set up such a thing in a public building you are not allowed to do it, because of the open flames
of the candles.

This is where my little project comes in. It drives 4 LEDs (get the flickering ones;)) depending of the current date. (The custom is to light up one candle on each advent sunday up to a total of 4 at the last sunday before christmas)

To make the advent wreath a little bit more usefull, I decided to built in another feature. As a teacher I know that many school lessons start late. Of course this is not always for the reason of a late teacher, but probably quite often ;)

I observed that the reason for this is easy: teachers do not have that much of a break. They talk to pupils, collegues, parents,.... all during school breaks. Therefore they forget about the next lesson and the long walk to the class room in another school building.

This is where my feature comes in:
one minute before the end of each school break the advent wreath will blink with all 4 candles. Just to inform teachers and pupils to go back to classes :) Oh and to check out the new BLYNK app, I created a nice little BLYNK app to control my advent manualy by phone. Nice for presentation.															
