/*
--------------------------------ESP8266 MOBILE CONTROLLED ADVENT WREATH ----------------------------|
|											 FOR SCHOOLS											|
|								 with auto mode and end-of-break alarm								|
|                                           by David Voß                                            |
|                                        David.Voss@bkukr.de                                        |
|                                  Berufskolleg Uerdingen, Krefeld									|
|
| In Germany the custom of having an advent wreath is very common. Unfortunately if you want to     |
| set up such a thing in a public building you are not allowed to do it, because of the open flames |
| of the candles.																					|
| This is where my little project comes in. It drives 4 LEDs (get the flickering ones;)) depending  |
| of the current date. (The custom is to light up one candle on each advent sunday up to a total	|
| of 4 at the last sunday before christmas)															|
| To make the advent wreath a little bit more usefull, I decided to built in another feature.		|
| As a teacher I know that many school lessons start late. Of course this is not always for the     |
| reason of a late teacher, but probably quite often ;)                                             |
| I observed that the reason for this is easy: teachers do not have that much of a break. They talk |
| to pupils, collegues, parents,.... all during school breaks. Therefore they forget about the next |
| lesson and the long walk to the class room in another school building.                            |
|                                                                                                   |
| This is where my feature comes in:																|
| one minute before the end of each school break the advent wreath will blink with all 4 candles.	|
| Just to inform teachers and pupils to go back to classes :)										|
| Oh and to check out the new BLYNK app, I created a nice little BLYNK app to control my advent		|
| manualy by phone. Nice for presentation.															|
|---------------------------------------------------------------------------------------------------|
*/
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <SimpleTimer.h>
#include <TimeLib.h>
#include <WidgetRTC.h>

//global settings
//#define BLYNK_PRINT Serial   // Comment this out to disable prints and save space
//use the BLYNK app to generate an API key and put it here
#define BLYNK_AUTH_CODE "XXX";

//I tried to setup the hostname, but for some reaseon it didn´t work
#define HOSTNAME "XXX"

//put your own WIFI settings in
#define WIFI_SSID "XXX"
#define WIFI_PASS "XXX"


//set to true, if you want to get an alarm if school break is coming
//to its end
const bool SCHOOL_BREAK_ALARM_ENABLED = true;
const int SCHOOL_BREAK_START_BLINK_WARNING_WHEN_X_BREAK_MINUTES_LEFT = 1;

//the advent wreath in auto mode
//will only work between these
//hours of each day. (24h format)
const int START_ADVENT_WREATH_AT = 7;
const int STOP_ADVENT_WREATH_AT = 22;

//physical candle pins
//these are ESP8266 pins.
//If you use NodeMCU you 
//have to change them
#define CANDLE_1_PIN 16
#define CANDLE_2_PIN 5
#define CANDLE_3_PIN 4
#define CANDLE_4_PIN 0

//virtual candle pins in your blynk app
#define CANDLE_1_VPIN V10
#define CANDLE_2_VPIN V11
#define CANDLE_3_VPIN V12
#define CANDLE_4_VPIN V13

//blynk virtual widget pins
#define WIDGET_RTC_VPIN V5
#define WIDGET_ACTLED_VPIN V6
#define WIDGET_AUTO_BTN_VPIN V4




void ExtinguishAllCandles(bool uploadToBlynk);

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = BLYNK_AUTH_CODE;

char ssid[] = WIFI_SSID;
char pass[] = WIFI_PASS;

bool isFirstConnect = true;
bool isStartupPhase = true;

//Vars für Adventskranz Logik
int DaysTillXmas = 0;
time_t AdventSundays[4];
bool AutomaticMode = true;
byte CandlePins[4] = { CANDLE_1_PIN,CANDLE_2_PIN,CANDLE_3_PIN,CANDLE_4_PIN };
byte CandleVPins[4] = { CANDLE_1_VPIN,CANDLE_2_VPIN,CANDLE_3_VPIN,CANDLE_4_VPIN };
byte TotalOfLitCandles = 0;
volatile bool LitCandleStatus[4] = { false,false,false,false };

//Vars for school break alarm
//Define school breaks (defined by start and duration: 09:00-09:25,10:55-...)
const byte schoolBreaksHoursAt[] = { 9, 10, 12, 18,19 };
const byte schoolBreaksMinutesAt[] = { 0, 55, 45, 20,50 };
const byte schoolBreaksMinutesDuration[] = { 25, 20, 15,10,10 };
volatile int schoolBreakCountdownMinutes = 0;
byte schoolBreakRunningIndex = 0;
bool schoolBreakIsOn = false; //is school braek on ?


unsigned long previousMillis = 0;
unsigned long previousBlinkMillis = 0;
long BlinkInterval = 500;
bool Blinking = false;

//Timer display refresh of clock every 15s
SimpleTimer timer;

//1 Second based imer to track some time values
SimpleTimer sectimer;

WidgetRTC rtc;
WidgetLED activeLED(WIDGET_ACTLED_VPIN);

//-------------------------------------
//Blynk methods
BLYNK_ATTACH_WIDGET(rtc, WIDGET_RTC_VPIN);

BLYNK_CONNECTED() {
	if (isFirstConnect) {
		Blynk.syncAll();
		isFirstConnect = false;
	}
}
//-------------------------------------
// Candle 1
BLYNK_WRITE(CANDLE_1_VPIN)
{
	BLYNK_LOG("Got a value for candle 1: %s", param.asStr());
	bool candle1 = param.asInt();
	if (!AutomaticMode)
	{
		digitalWrite(CANDLE_1_PIN, candle1);
		LitCandleStatus[0] = candle1;

	}
	else
	{
		Blynk.virtualWrite(CANDLE_1_VPIN, !candle1);
	}

}
//-------------------------------------
// Candle 2
BLYNK_WRITE(CANDLE_2_VPIN)
{
	BLYNK_LOG("Got a value for candle 2: %s", param.asStr());
	bool candle1 = param.asInt();
	if (!AutomaticMode)
	{
		digitalWrite(CANDLE_2_PIN, candle1);
		LitCandleStatus[2] = candle1;
	}
	else
	{
		Blynk.virtualWrite(CANDLE_2_VPIN, !candle1);
	}

}
//-------------------------------------
// Candle 3
BLYNK_WRITE(CANDLE_3_VPIN)
{
	BLYNK_LOG("Got a value for candle 3: %s", param.asStr());
	bool candle1 = param.asInt();
	if (!AutomaticMode)
	{
		digitalWrite(CANDLE_3_PIN, candle1);
		LitCandleStatus[2] = candle1;
	}
	else
	{
		Blynk.virtualWrite(CANDLE_3_VPIN, !candle1);
	}

}
//-------------------------------------
// Candle 4
BLYNK_WRITE(CANDLE_4_VPIN)
{
	BLYNK_LOG("Got a value for candle 4: %s", param.asStr());
	bool candle1 = param.asInt();
	if (!AutomaticMode)
	{
		digitalWrite(CANDLE_4_PIN, candle1);
		LitCandleStatus[3] = candle1;
	}
	else
	{
		Blynk.virtualWrite(CANDLE_4_VPIN, !candle1);
	}

}
//-------------------------------------
// auto mode switch
BLYNK_WRITE(WIDGET_AUTO_BTN_VPIN)
{
	if (!isStartupPhase)
	{
		AutomaticMode = param.asInt();
		if (AutomaticMode)
			EnableAutoMode();
		else
			DisableAutoMode();

	}
}

//-------------------------------------
//Setup  Adventskranzes. 
void setup()
{
	pinMode(CANDLE_1_PIN, OUTPUT);
	pinMode(CANDLE_2_PIN, OUTPUT);
	pinMode(CANDLE_3_PIN, OUTPUT);
	pinMode(CANDLE_4_PIN, OUTPUT);

	//Initialisierung
	Serial.begin(9600);
	Blynk.begin(auth, ssid, pass);

	while (Blynk.connect() == false) {
		// Wait until connected
	}

	//Show IP, nice if you have to configure FW
	Serial.print("Adventskranz IP: ");
	Serial.println(WiFi.localIP());


	activeLED.off();

	// Begin synchronizing time (asynchronous!)
	rtc.begin();

	// Display digital clock every 15 seconds
	timer.setInterval(15000L, clockDisplay);
	// 1 second timer
	sectimer.setInterval(1000L, clockUpdate);

}
//-------------------------------------
//Start Automode 
void EnableAutoMode()
{
	ExtinguishAllCandles(true);
	AutoMode();
	Blynk.virtualWrite(WIDGET_AUTO_BTN_VPIN, true);
	AutomaticMode = true;
	Serial.println("Automode Enabled!");
}

//Stop Automode (usually by Blynk Button event)
void DisableAutoMode()
{
	ExtinguishAllCandles(true);
	Blynk.virtualWrite(WIDGET_AUTO_BTN_VPIN, false);
	AutomaticMode = false;
	Serial.println("Automode Disabled!");
}
//-------------------------------------
//Adventskranz lights candles depending on advent
//and time of day
void AutoMode()
{
	//Check, if we are between ON hours
	if (hour() >= START_ADVENT_WREATH_AT && hour() < STOP_ADVENT_WREATH_AT)
	{
		if (now() > AdventSundays[0] && TotalOfLitCandles < 1)
		{
			digitalWrite(CANDLE_1_PIN, HIGH);
			TotalOfLitCandles++;
			LitCandleStatus[0] = true;
			Blynk.virtualWrite(CANDLE_1_VPIN, true);
			Serial.println("1.Advent !");
		}
		if (now() > AdventSundays[1] && TotalOfLitCandles < 2)
		{

			digitalWrite(CANDLE_2_PIN, HIGH);
			TotalOfLitCandles++;
			LitCandleStatus[1] = true;
			Blynk.virtualWrite(CANDLE_2_VPIN, true);
			Serial.println("2.Advent !");
		}
		if (now() > AdventSundays[2] && TotalOfLitCandles < 3)
		{
			digitalWrite(CANDLE_3_PIN, HIGH);
			TotalOfLitCandles++;
			LitCandleStatus[2] = true;
			Blynk.virtualWrite(CANDLE_3_VPIN, true);
			Serial.println("3.Advent !");

		}
		if (now() > AdventSundays[3] && TotalOfLitCandles < 4)
		{
			digitalWrite(CANDLE_4_PIN, HIGH);
			TotalOfLitCandles++;
			LitCandleStatus[3] = true;
			Blynk.virtualWrite(CANDLE_4_VPIN, true);
			Serial.println("4.Advent !");

		}
	}
	else
	{
		ExtinguishAllCandles(true);
	}

}
//-------------------------------------
//Extinguish all candles
//not always refelecting to Blynk server
void ExtinguishAllCandles(bool uploadToBlynk = true)
{
	Serial.println("ExtinguishAllCandles!");
	for (byte Kerze = 0; Kerze < 4; Kerze++)
	{
		digitalWrite(CandlePins[Kerze], LOW);
		LitCandleStatus[Kerze] = false;
		if (uploadToBlynk) Blynk.virtualWrite(CandleVPins[Kerze], false);
	}
	TotalOfLitCandles = 0;
}
//-------------------------------------
//Blink to alarm end of echool break is near
void Blink()
{
	digitalWrite(CANDLE_1_PIN, !digitalRead(CANDLE_1_PIN));
	digitalWrite(CANDLE_2_PIN, !digitalRead(CANDLE_2_PIN));
	digitalWrite(CANDLE_3_PIN, !digitalRead(CANDLE_3_PIN));
	digitalWrite(CANDLE_4_PIN, !digitalRead(CANDLE_4_PIN));
}
//-------------------------------------
//main loop
void loop()
{

	Blynk.run();
	timer.run();
	sectimer.run();

	//Blinking of Adventskranz if Blinking enabled
	unsigned long currentMillis = millis();

	if (Blinking && (currentMillis - previousBlinkMillis >= BlinkInterval))
	{
		Blink();
		previousBlinkMillis = currentMillis;
	}
}
//-------------------------------------
//Calculates remaining days till 12/24
int GetChristmasCountdownInDays()
{
	time_t Heiligabend;
	time_t SecsBisWeihnachten;
	time_t RestSekunden;
	tmElements_t tmeH;


	tmeH.Second = 0;
	tmeH.Minute = 0;
	tmeH.Hour = 0;
	tmeH.Day = 24; //you shuold change this to 25, if you´re celebrating christmas morning
	tmeH.Month = 12;
	tmeH.Year = year() - 1970;

	Heiligabend = makeTime(tmeH);
	CalculateAdventSundays(Heiligabend, weekday(Heiligabend));
	SecsBisWeihnachten = Heiligabend - now();
	RestSekunden = SecsBisWeihnachten % SECS_PER_DAY;
	SecsBisWeihnachten = SecsBisWeihnachten - RestSekunden;
	DaysTillXmas = (SecsBisWeihnachten / SECS_PER_DAY) + 1;
	return DaysTillXmas;
}
//-------------------------------------
//Calc dates of all 4 advent sundays
void CalculateAdventSundays(time_t lHolyEvening, byte Weekday)
{
	//we need sunday = day 0, not day 1 of week
	Weekday--;
	//calculate 4th advent and from there all other sundays
	AdventSundays[3] = lHolyEvening - (SECS_PER_DAY * Weekday);
	AdventSundays[2] = AdventSundays[3] - (SECS_PER_WEEK);
	AdventSundays[1] = AdventSundays[2] - (SECS_PER_WEEK);
	AdventSundays[0] = AdventSundays[1] - (SECS_PER_WEEK);
}

//-------------------------------------
//get number of advent right now
int CalcNowAdvent()
{

	//Aktuellen Advent berechnen
	int advent = 0;
	for (int i = 0; i < 4; i++)
	{
		if (now() > AdventSundays[i]) advent++;
	}
	return advent;
}
//-------------------------------------
//Every second to update some vars
void clockUpdate()
{
	//Auf Schulpause prüfen...

	if (check4break(hour(), minute()))
	{
		if (second() == 0)
		{
			schoolBreakCountdownMinutes--;
		}
		if ((schoolBreakCountdownMinutes == SCHOOL_BREAK_START_BLINK_WARNING_WHEN_X_BREAK_MINUTES_LEFT - 1) && SCHOOL_BREAK_ALARM_ENABLED)
		{
			Blinking = true;
		}
		//Serial.println("Es sind noch " + String(schoolBreakCountdownMinutes) + " min Pause...");

	}
}
//-------------------------------------
// Digital clock display of the time
void clockDisplay()
{
	char currentTime[5];
	char currentDate[12];
	//time synced and startup phase done ?
	//if not -> return
	if (timeStatus() == timeNotSet && isStartupPhase)
	{
		Serial.println("waiting for timesync...");
		activeLED.off();
		//time was not synced->get outa here
		return;
	}
	//time is set but we are still during startupphase
	else if (timeStatus() == timeSet && isStartupPhase)
	{
		isStartupPhase = false;
		//enable automode
		Serial.println("time set, enabling AutoMode...");
		// calc christmas countdown
		DaysTillXmas = GetChristmasCountdownInDays();
		EnableAutoMode();
		activeLED.on();
	}
	else if (timeStatus() == timeSet && !isStartupPhase && AutomaticMode)
	{
		//important to light another candle if we have 
		//the next advent and to check if we are between
		// ON hours
		AutoMode();
	}



	sprintf(currentTime, "%02d:%02d", hour(), minute());
	sprintf(currentDate, "%02d.%02d.%4d", day(), month(), year());


	int advent = CalcNowAdvent();

	// Weihnachtstage
	Blynk.virtualWrite(V0, DaysTillXmas);
	// Send time to the App
	Blynk.virtualWrite(V1, currentTime);
	// Send date to the App
	Blynk.virtualWrite(V2, currentDate);
	//which advent is currently on?
	Blynk.virtualWrite(V3, (advent == 0) ? "-" : (String(advent) + ". Advent"));

	//serial output
	Serial.print(currentTime);
	Serial.print("/");
	Serial.print(currentDate);
	Serial.print(":");
	Serial.print(String(advent) + ". Advent, ");
	Serial.println(String(DaysTillXmas) + "CC, Pause: " + String(schoolBreakIsOn)+", Alarm: "+String(Blinking));



}
//-------------------------------------
//flash Adventskranz LEDs, if teachers have to go back to classes ;)
void SchoolBreakLightSignal()
{
	//ExtinguishAllCandles(false);
	//10 Runden
	for (byte i = 0; i < 10; i++)
	{
		for (byte j = 0; j < 4; j++)
		{
			(i % 2 == 1) ? LightCandle(j, false) : ExtCandle(j, false);
			delay(200 * j);
		}

	}
	RestoreCandleStates();
}
//-------------------------------------
//Extinguish a single candle
void ExtCandle(byte candle, bool uploadToBlynk)
{
	digitalWrite(CandlePins[candle], false);
	if (uploadToBlynk) Blynk.virtualWrite(CandleVPins[candle], false);

}
//-------------------------------------
//Light up single candle
void LightCandle(byte candle, bool uploadToBlynk)
{
	digitalWrite(CandlePins[candle], true);
	if (uploadToBlynk) Blynk.virtualWrite(CandleVPins[candle], true);

}
//-------------------------------------
//Restore candle states
void RestoreCandleStates()
{
	for (byte candle = 0; candle < 4; candle++)
	{
		digitalWrite(CandlePins[candle], LitCandleStatus[candle]);
	}

}
//-------------------------------------
//check, if we are having a school break right now or not.
bool check4break(byte hours, byte minutes)
{
	if (schoolBreakIsOn)
	{
		//Serial.println("Pause laeuft...");
		if (schoolBreakCountdownMinutes < 0)
		{
			schoolBreakIsOn = false;
			Blinking = false;
			schoolBreakCountdownMinutes = 0;
			Serial.println("Pause beendet!");
			RestoreCandleStates();
			return false;
		}
		else
			return true;
	}
	else
	{
		//Serial.println("Keine Pause...");
		for (byte i = 0; i < sizeof(schoolBreaksHoursAt); i++)
		{
			if ((hour() == schoolBreaksHoursAt[i]) && (minute() == schoolBreaksMinutesAt[i]))
			{
				schoolBreakCountdownMinutes = schoolBreaksMinutesDuration[i];
				schoolBreakRunningIndex = i;
				schoolBreakIsOn = true;
				Serial.print("Pause beginnt! Es ist ");
				Serial.print(schoolBreaksHoursAt[i]);
				Serial.print(":");
				Serial.print(schoolBreaksMinutesAt[i]);
				Serial.print("/");
				Serial.println(schoolBreaksMinutesDuration[i]);
				return true;
			}
		}
		return false;
	}
}
